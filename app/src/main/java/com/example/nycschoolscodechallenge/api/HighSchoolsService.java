package com.example.nycschoolscodechallenge.api;

import com.example.nycschoolscodechallenge.BuildConfig;
import com.example.nycschoolscodechallenge.api.details.entity.HighSchoolSatScoresResponseItem;
import com.example.nycschoolscodechallenge.api.main.entity.HighSchoolResponseItem;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HighSchoolsService {

    @GET(BuildConfig.ENDPOINT_HIGH_SCHOOLS)
    Single<List<HighSchoolResponseItem>> getHighSchoolsSingle(
            @Query(value = "$$app_token", encoded = true) String key,
            @Query(value = "$limit") int limit,
            @Query(value = "$offset") int offset
    );

    @GET(BuildConfig.ENDPOINT_SCHOOL_SAT_SCORES)
    Single<List<HighSchoolSatScoresResponseItem>> getHighSchoolSatScores(
            @Query(value = "$$app_token", encoded = true) String key,
            @Query(value = "dbn", encoded = true) String dbn
    );

}
