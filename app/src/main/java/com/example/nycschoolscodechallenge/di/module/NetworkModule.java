package com.example.nycschoolscodechallenge.di.module;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.nycschoolscodechallenge.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private static final String TAG_LOGGING_INTERCEPTOR = "OkHttp";

    @Provides
    static Retrofit provideRetroFit(
            HttpUrl httpUrl,
            GsonConverterFactory gsonConverterFactory,
            RxJava2CallAdapterFactory rxJava2CallAdapterFactory,
            OkHttpClient okHttpClient
    ) {
        return new Retrofit.Builder()
                       .baseUrl(httpUrl)
                       .client(okHttpClient)
                       .addConverterFactory(gsonConverterFactory)
                       .addCallAdapterFactory(rxJava2CallAdapterFactory)
                       .build();
    }

    @Provides
    static Gson provideGson() {

        return new GsonBuilder().create();
    }

    @Provides
    static GsonConverterFactory provideGsonConverterFactory(@NonNull Gson gson) {

        return GsonConverterFactory.create(gson);
    }

    @Provides
    static RxJava2CallAdapterFactory provideRxJavaCallAdapterFactory() {

        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    static OkHttpClient provideOkHttpClient(@NonNull HttpLoggingInterceptor httpLoggingInterceptor) {

        return new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();
    }

    @Provides
    static HttpLoggingInterceptor provideHttpLoggingInterceptor() {

        return new HttpLoggingInterceptor(message -> Log.d(TAG_LOGGING_INTERCEPTOR, message))
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    static HttpUrl provideHttpUrl() {

        return HttpUrl.parse(BuildConfig.BASE_URL);
    }

}

