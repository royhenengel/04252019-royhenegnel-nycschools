package com.example.nycschoolscodechallenge.di.module;

import androidx.annotation.NonNull;

import com.example.nycschoolscodechallenge.api.HighSchoolsService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {NetworkModule.class})
public class ApiModule {

    @Provides
    static HighSchoolsService provideHighSchoolsService(@NonNull Retrofit retrofit){

        return retrofit.create(HighSchoolsService.class);
    }
}
