package com.example.nycschoolscodechallenge.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


import com.example.nycschoolscodechallenge.di.ViewModelKey;
import com.example.nycschoolscodechallenge.ui.details.viewmodel.HighSchoolSatScoresViewModel;
import com.example.nycschoolscodechallenge.ui.main.viewmodel.HighSchoolsFragmentViewModel;
import com.example.nycschoolscodechallenge.viewmodel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(HighSchoolsFragmentViewModel.class)
    abstract ViewModel bindHighSchoolsFragmentViewModel(HighSchoolsFragmentViewModel highSchoolsFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HighSchoolSatScoresViewModel.class)
    abstract ViewModel bindHighSchoolsSatScoresViewModel(HighSchoolSatScoresViewModel highSchoolsFragmentViewModel);

}
