package com.example.nycschoolscodechallenge.di;

import android.app.Application;

import com.example.nycschoolscodechallenge.App;
import com.example.nycschoolscodechallenge.di.module.ActivityBuildersModule;
import com.example.nycschoolscodechallenge.di.module.ApiModule;
import com.example.nycschoolscodechallenge.di.module.DataModule;
import com.example.nycschoolscodechallenge.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        ActivityBuildersModule.class,
        ViewModelModule.class,
        ApiModule.class,
        DataModule.class
})

public interface AppComponent {

    void inject(App app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
