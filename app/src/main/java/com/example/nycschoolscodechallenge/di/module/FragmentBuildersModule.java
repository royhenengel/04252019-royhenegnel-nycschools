package com.example.nycschoolscodechallenge.di.module;

import com.example.nycschoolscodechallenge.ui.details.view.HighSchoolSatScoresFragment;
import com.example.nycschoolscodechallenge.ui.main.view.HighSchoolsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract HighSchoolsFragment contributeHighSchoolsFragment();

    @ContributesAndroidInjector
    abstract HighSchoolSatScoresFragment contributeHighSchoolsSatScoresFragment();

}
