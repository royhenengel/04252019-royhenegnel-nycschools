package com.example.nycschoolscodechallenge.ui.main.entity.mapper;

import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.ui.main.entity.HighSchoolUiEntity;

import javax.inject.Inject;

import io.reactivex.functions.Function;

public class HighSchoolEntityToUiEntityMapper implements Function<HighSchoolEntity, HighSchoolUiEntity> {

    private static final String PATTERN_ADDRESS = "%s, %s %s %s";

    @Inject
    public HighSchoolEntityToUiEntityMapper() { }

    @Override
    public HighSchoolUiEntity apply(HighSchoolEntity highSchoolEntity) {

        return new HighSchoolUiEntity(
                highSchoolEntity.getId(),
                highSchoolEntity.getName(),
                String.format(PATTERN_ADDRESS, highSchoolEntity.getStreet(), highSchoolEntity.getCity(),
                              highSchoolEntity.getStateCode(), highSchoolEntity.getZipCode()),
                highSchoolEntity.getWebsite(),
                highSchoolEntity.getOverview()
        );
    }

}
