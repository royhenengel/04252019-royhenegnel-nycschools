package com.example.nycschoolscodechallenge.ui.main.view;

import androidx.constraintlayout.widget.Group;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.nycschoolscodechallenge.R;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.di.Injectable;
import com.example.nycschoolscodechallenge.ui.main.entity.HighSchoolUiEntity;
import com.example.nycschoolscodechallenge.ui.main.entity.mapper.HighSchoolEntityToUiEntityMapper;
import com.example.nycschoolscodechallenge.ui.main.viewmodel.HighSchoolsFragmentViewModel;
import com.example.nycschoolscodechallenge.viewmodel.ViewModelFactory;

import javax.inject.Inject;

public class HighSchoolsFragment extends Fragment implements Injectable,
                                                             MainHighSchoolViewHolder.HighSchoolClickListener {

    @Inject
    ViewModelFactory viewModelFactory;

    @SuppressWarnings("WeakerAccess")
    @Inject
    HighSchoolEntityToUiEntityMapper highSchoolEntityToUiEntityMapper;

    private HighSchoolsFragmentViewModel fragmentViewModel;
    private NavController navController;
    private RecyclerView highSchoolsRv;
    private ProgressBar loader;
    private Group errorViewGroup;
    private TextView errorTv;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_high_schools, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        setListeners(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        navController = NavHostFragment.findNavController(this);
        fragmentViewModel = ViewModelProviders.of(this, viewModelFactory).get(HighSchoolsFragmentViewModel.class);
        fragmentViewModel.start();

        observe();
    }

    @Override
    public void onHighSchoolClicked(@NonNull HighSchoolUiEntity highSchoolUiEntity) {

        fragmentViewModel.onHighSchoolClicked(highSchoolUiEntity);
    }

    private void initView(@NonNull View view) {

        initRecyclerView(view);
        loader = view.findViewById(R.id.high_schools_pb_loader);
        errorViewGroup = view.findViewById(R.id.high_schools_group_error_views);
        errorTv = view.findViewById(R.id.high_schools_tv_error);
    }

    private void initRecyclerView(@NonNull View view) {

        highSchoolsRv = view.findViewById(R.id.high_schools_rv);
        highSchoolsRv.setHasFixedSize(true);
        highSchoolsRv.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void setListeners(View view) {

        view.findViewById(R.id.high_schools_btn_try_again).setOnClickListener(v -> fragmentViewModel.onTryAgainBtnClicked());
    }

    private void observe() {

        fragmentViewModel.getHighSchoolsPagedLiveData().observe(this, this::handleHighSchoolData);
        fragmentViewModel.getStateLiveData().observe(this, this::handleStateChangedData);
        fragmentViewModel.getUserErrorMessageLiveData().observe(this, this::handleUserErrorMessageData);
        fragmentViewModel.getOnHighSchoolClickedLiveEvent().observe(this, this::handleHighSchoolClickedEvent);
    }

    private void handleHighSchoolData(@NonNull PagedList<HighSchoolEntity> highSchoolEntities) {

        MainHighSchoolsAdapter adapter = new MainHighSchoolsAdapter(highSchoolEntityToUiEntityMapper,
                                                                    this);
        highSchoolsRv.setAdapter(adapter);
        adapter.submitList(highSchoolEntities);
    }

    private void handleStateChangedData(@NonNull HighSchoolsFragmentViewModel.STATE state) {

        switch (state){

            case LOADING:
                showLoading();
                break;

            case LOADED:
                showLoaded();
                break;

            case ERROR:
                showError();
                break;
        }
    }

    private void handleUserErrorMessageData(String message) {

        errorTv.setText(message);
    }

    private void handleHighSchoolClickedEvent(HighSchoolUiEntity uiEntity) {

        final HighSchoolsFragmentDirections.ActionDestHighSchoolsToHighSchoolsSatScores action =
                HighSchoolsFragmentDirections.actionDestHighSchoolsToHighSchoolsSatScores(
                        uiEntity.getId(), uiEntity.getAddress(), uiEntity.getWebsite());

        navController.navigate(action);
    }

    private void showLoading(){

        highSchoolsRv.setVisibility(View.INVISIBLE);
        errorViewGroup.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
    }

    private void showError(){

        highSchoolsRv.setVisibility(View.INVISIBLE);
        loader.setVisibility(View.GONE);
        errorViewGroup.setVisibility(View.VISIBLE);
    }

    private void showLoaded(){

        errorViewGroup.setVisibility(View.GONE);
        loader.setVisibility(View.GONE);
        highSchoolsRv.setVisibility(View.VISIBLE);
    }

}
