package com.example.nycschoolscodechallenge.ui.main.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;

import com.example.nycschoolscodechallenge.R;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.ui.main.entity.HighSchoolUiEntity;
import com.example.nycschoolscodechallenge.ui.main.entity.mapper.HighSchoolEntityToUiEntityMapper;

public class MainHighSchoolsAdapter extends PagedListAdapter<HighSchoolEntity, MainHighSchoolViewHolder> {

    private final HighSchoolEntityToUiEntityMapper uiEntityMapper;
    private final MainHighSchoolViewHolder.HighSchoolClickListener highSchoolClickListener;

    protected MainHighSchoolsAdapter(@NonNull HighSchoolEntityToUiEntityMapper uiEntityMapper,
                                     @Nullable MainHighSchoolViewHolder.HighSchoolClickListener highSchoolClickListener) {
        super(diffCallback);

        this.uiEntityMapper = uiEntityMapper;
        this.highSchoolClickListener = highSchoolClickListener;
    }

    @NonNull
    @Override
    public MainHighSchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                       int viewType) {
        View rootView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_high_school, parent, false);

        return new MainHighSchoolViewHolder(rootView, highSchoolClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MainHighSchoolViewHolder holder,
                                 int position) {

        final HighSchoolUiEntity uiEntity = uiEntityMapper.apply(getItem(position));
        holder.bind(uiEntity);
    }

    private static final DiffUtil.ItemCallback<HighSchoolEntity> diffCallback =
            new DiffUtil.ItemCallback<HighSchoolEntity>() {

        @Override
        public boolean areItemsTheSame(@NonNull HighSchoolEntity oldItem,
                                       @NonNull HighSchoolEntity newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull HighSchoolEntity oldItem,
                                          @NonNull HighSchoolEntity newItem) {
            return oldItem.equals(newItem);
        }
    };
}
