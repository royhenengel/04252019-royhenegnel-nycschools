package com.example.nycschoolscodechallenge.ui.details.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.nycschoolscodechallenge.data.HighSchoolsRepository;
import com.example.nycschoolscodechallenge.ui.details.entity.HighSchoolSatScoresUiEntity;
import com.example.nycschoolscodechallenge.ui.details.entity.mapper.HighSchoolScoresEntityToUiEntityMapper;
import com.example.nycschoolscodechallenge.viewmodel.SingleLiveEvent;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HighSchoolSatScoresViewModel extends ViewModel {

    private static final String ERROR_MESSAGE_SCHOOL_NOT_AVAILABLE = "School not available";
    private static final String ERROR_MESSAGE_GENERAL_MESSAGE = "Error loading data\nPlease " +
            "check your internet connection\nand try again";
    private static final String URL_PREFIX = "http://";

    private final HighSchoolsRepository repository;
    private final HighSchoolScoresEntityToUiEntityMapper entityToUiEntityMapper;

    private MutableLiveData<HighSchoolSatScoresUiEntity> uiEntityLiveData;
    private MutableLiveData<String> errorMessageLiveData;
    private MutableLiveData<STATE> stateLiveData;

    private SingleLiveEvent<String> onWebSiteClickedLiveEvent;

    private String schoolId;
    private Disposable disposable;

    private boolean shouldShowTryAgainBtn;

    @Inject
    public HighSchoolSatScoresViewModel(@NonNull HighSchoolsRepository repository,
                                        @NonNull HighSchoolScoresEntityToUiEntityMapper entityToUiEntityMapper) {

        this.repository = repository;
        this.entityToUiEntityMapper = entityToUiEntityMapper;

        this.uiEntityLiveData = new MutableLiveData<>();
        this.errorMessageLiveData = new MutableLiveData<>(ERROR_MESSAGE_GENERAL_MESSAGE);
        this.stateLiveData = new MutableLiveData<>();
        this.onWebSiteClickedLiveEvent = new SingleLiveEvent<>();
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        if (disposable != null) {
            disposable.dispose();
        }
    }

    public void start(String id, String address, String webSite) {

        stateLiveData.setValue(STATE.LOADING);

        this.schoolId = id;
        entityToUiEntityMapper.setAddress(address);
        entityToUiEntityMapper.setWebSite(webSite);
        initData();
    }

    public void onTryAgainBtnClicked() {

        stateLiveData.setValue(STATE.LOADING);
        initData();
    }

    public MutableLiveData<HighSchoolSatScoresUiEntity> getUiEntityLiveData() {

        return uiEntityLiveData;
    }

    public MutableLiveData<String> getErrorMessageLiveData() {
        return errorMessageLiveData;
    }

    public MutableLiveData<STATE> getStateLiveData() {
        return stateLiveData;
    }

    public SingleLiveEvent<String> getOnWebSiteClickedLiveEvent() {

        return onWebSiteClickedLiveEvent;
    }

    public boolean shouldShowTryAgainBtn() {

        return shouldShowTryAgainBtn;
    }

    private void initData() {

        disposable = repository.getHighSchoolScores(schoolId)
                               .subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .map(entityToUiEntityMapper)
                               .subscribe(this::handleHighSchoolScoresData, this::handleError);
    }

    private void handleHighSchoolScoresData(HighSchoolSatScoresUiEntity uiEntity) {

        uiEntityLiveData.setValue(uiEntity);
        stateLiveData.setValue(STATE.LOADED);
    }

    private void handleError(Throwable throwable) {

        if (throwable instanceof IndexOutOfBoundsException){
            errorMessageLiveData.setValue(ERROR_MESSAGE_SCHOOL_NOT_AVAILABLE);
            shouldShowTryAgainBtn = false;
        }
        else {
            errorMessageLiveData.setValue(ERROR_MESSAGE_GENERAL_MESSAGE);
            shouldShowTryAgainBtn = true;
        }

        stateLiveData.setValue(STATE.ERROR);
    }

    public void onWebsiteClicked() {

        String originalUrl = uiEntityLiveData.getValue().getWebSite();
        String url = new StringBuilder(originalUrl).insert(0, URL_PREFIX).toString();
        onWebSiteClickedLiveEvent.setValue(url);
    }

    public enum STATE {

        LOADING, LOADED, ERROR
    }

}
