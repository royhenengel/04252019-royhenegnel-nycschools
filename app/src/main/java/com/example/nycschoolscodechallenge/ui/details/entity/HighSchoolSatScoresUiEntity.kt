package com.example.nycschoolscodechallenge.ui.details.entity

data class HighSchoolSatScoresUiEntity (

        val name: String,

        val address: String,

        val writingScore: String,

        val readingScore: String,

        val mathScore: String,

        val webSite: String

)