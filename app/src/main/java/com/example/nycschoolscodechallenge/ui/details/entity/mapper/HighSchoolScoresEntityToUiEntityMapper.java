package com.example.nycschoolscodechallenge.ui.details.entity.mapper;

import com.example.nycschoolscodechallenge.data.entity.details.HighSchoolSatScoresEntity;
import com.example.nycschoolscodechallenge.ui.details.entity.HighSchoolSatScoresUiEntity;

import javax.inject.Inject;

import io.reactivex.functions.Function;

public class HighSchoolScoresEntityToUiEntityMapper implements Function<HighSchoolSatScoresEntity, HighSchoolSatScoresUiEntity> {

    private static final String URL_PREFIX = "www";

    private String address;
    private String webSite;

    @Inject
    public HighSchoolScoresEntityToUiEntityMapper() { }

    @Override
    public HighSchoolSatScoresUiEntity apply(HighSchoolSatScoresEntity entity) {

        return new HighSchoolSatScoresUiEntity(
                entity.getSchoolName(),
                address == null ? "" : address,
                entity.getWritingAvgScore(),
                entity.getReadingAvgScore(),
                entity.getMathAvgScore(),
                webSite == null ? "" : webSite);
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public void setWebSite(String webSite) {

        final String prefix = new StringBuilder(webSite).substring(0, 3);
        if (prefix.equals(URL_PREFIX)){
            this.webSite = webSite;
        }
    }
}
