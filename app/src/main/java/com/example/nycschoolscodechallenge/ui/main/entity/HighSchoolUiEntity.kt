package com.example.nycschoolscodechallenge.ui.main.entity

data class HighSchoolUiEntity (

        val id: String,

        val name: String,

        val address: String,

        val website: String,

        val overview: String

)