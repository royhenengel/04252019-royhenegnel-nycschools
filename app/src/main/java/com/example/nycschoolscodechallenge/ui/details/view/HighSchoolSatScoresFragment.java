package com.example.nycschoolscodechallenge.ui.details.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.nycschoolscodechallenge.R;
import com.example.nycschoolscodechallenge.di.Injectable;
import com.example.nycschoolscodechallenge.ui.details.entity.HighSchoolSatScoresUiEntity;
import com.example.nycschoolscodechallenge.ui.details.viewmodel.HighSchoolSatScoresViewModel;
import com.example.nycschoolscodechallenge.viewmodel.ViewModelFactory;
import com.google.android.material.button.MaterialButton;

import javax.inject.Inject;

public class HighSchoolSatScoresFragment extends Fragment implements Injectable {

    @Inject
    ViewModelFactory viewModelFactory;

    private HighSchoolSatScoresViewModel fragmentViewModel;

    private ProgressBar loaderPb;
    private Group errorViewsGroup, layoutViewsGroup, websiteViewGroup;
    private TextView nameTv, addressTv, writingScoreTv, readingScoreTv, mathScoreTv, websiteTv, errorMessageTv;
    private MaterialButton tryAgainBtn;

    private boolean showWebSite;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_high_schoold_sat_scores, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        setListeners();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final String id = HighSchoolSatScoresFragmentArgs.fromBundle(getArguments()).getId();
        final String address = HighSchoolSatScoresFragmentArgs.fromBundle(getArguments()).getAddress();
        final String webSite = HighSchoolSatScoresFragmentArgs.fromBundle(getArguments()).getWebsite();
        fragmentViewModel = ViewModelProviders.of(this, viewModelFactory).get(HighSchoolSatScoresViewModel.class);
        fragmentViewModel.start(id, address, webSite);

        observe();
    }

    private void initView(View view) {

        loaderPb = view.findViewById(R.id.school_scores_pb_loader);
        errorViewsGroup = view.findViewById(R.id.school_scores_group_error_views);
        layoutViewsGroup = view.findViewById(R.id.school_scores_group_layout_views);
        nameTv = view.findViewById(R.id.school_scores_tv_name);
        addressTv = view.findViewById(R.id.school_scores_tv_address);
        writingScoreTv = view.findViewById(R.id.school_scores_tv_score_reading_body);
        readingScoreTv = view.findViewById(R.id.school_scores_tv_score_writing_body);
        mathScoreTv = view.findViewById(R.id.school_scores_tv_score_math_body);
        errorMessageTv = view.findViewById(R.id.school_scores_tv_error);
        websiteViewGroup = view.findViewById(R.id.school_scores_group_website_views);
        websiteTv = view.findViewById(R.id.school_scores_tv_website_body);
        tryAgainBtn = view.findViewById(R.id.school_scores_btn_try_again);
    }

    private void setListeners() {

        tryAgainBtn.setOnClickListener(v -> fragmentViewModel.onTryAgainBtnClicked());
    }

    private void observe() {

        fragmentViewModel.getErrorMessageLiveData().observe(this, this::handleErrorMessageData);
        fragmentViewModel.getStateLiveData().observe(this, this::handleStateChangedData);
        fragmentViewModel.getUiEntityLiveData().observe(this, this::handleUiEntityData);
        fragmentViewModel.getOnWebSiteClickedLiveEvent().observe(this, this::handleWebsiteClickedEvent);
    }

    private void handleUiEntityData(@NonNull HighSchoolSatScoresUiEntity uiEntity) {

        nameTv.setText(uiEntity.getName());
        addressTv.setText(uiEntity.getAddress());
        writingScoreTv.setText(uiEntity.getWritingScore());
        readingScoreTv.setText(uiEntity.getReadingScore());
        mathScoreTv.setText(uiEntity.getMathScore());

        if (!uiEntity.getWebSite().isEmpty()){
            showWebSite = true;
            websiteTv.setText(uiEntity.getWebSite());
            websiteTv.setOnClickListener(v -> fragmentViewModel.onWebsiteClicked());
        }
    }

    private void handleErrorMessageData(@NonNull String message) {

        errorMessageTv.setText(message);
    }

    private void handleStateChangedData(@NonNull HighSchoolSatScoresViewModel.STATE state) {

        switch (state) {

            case LOADING:
                showLoading();
                break;

            case LOADED:
                showLoaded();
                break;

            case ERROR:
                showError();
                break;

            default:
        }
    }

    private void handleWebsiteClickedEvent(String url) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    private void showLoading() {

        errorViewsGroup.setVisibility(View.INVISIBLE);
        layoutViewsGroup.setVisibility(View.GONE);
        websiteViewGroup.setVisibility(View.GONE);
        tryAgainBtn.setVisibility(View.GONE);
        loaderPb.setVisibility(View.VISIBLE);
    }

    private void showError() {

        layoutViewsGroup.setVisibility(View.INVISIBLE);
        websiteViewGroup.setVisibility(View.GONE);
        loaderPb.setVisibility(View.GONE);
        errorViewsGroup.setVisibility(View.VISIBLE);

        if (fragmentViewModel.shouldShowTryAgainBtn()){
            tryAgainBtn.setVisibility(View.VISIBLE);
        }
        else {
            tryAgainBtn.setVisibility(View.GONE);
        }
    }

    private void showLoaded() {

        errorViewsGroup.setVisibility(View.GONE);
        tryAgainBtn.setVisibility(View.GONE);
        loaderPb.setVisibility(View.GONE);
        layoutViewsGroup.setVisibility(View.VISIBLE);

        if (showWebSite){
            websiteViewGroup.setVisibility(View.VISIBLE);
        }
    }

}
