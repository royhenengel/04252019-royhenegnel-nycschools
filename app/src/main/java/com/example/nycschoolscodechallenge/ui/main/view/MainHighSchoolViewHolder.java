package com.example.nycschoolscodechallenge.ui.main.view;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschoolscodechallenge.R;
import com.example.nycschoolscodechallenge.ui.main.entity.HighSchoolUiEntity;

class MainHighSchoolViewHolder extends RecyclerView.ViewHolder {

    private HighSchoolClickListener clickListener;

    private ConstraintLayout rootCl;
    private TextView nameTv, addressTv, overviewTv;

    interface HighSchoolClickListener{
        void onHighSchoolClicked(@NonNull HighSchoolUiEntity highSchoolUiEntity);
    }

    MainHighSchoolViewHolder(@NonNull View itemView,
                             @Nullable HighSchoolClickListener highSchoolClickListener) {
        super(itemView);

        this.clickListener = highSchoolClickListener;

        rootCl = itemView.findViewById(R.id.item_high_school_cl_root);
        nameTv = itemView.findViewById(R.id.item_high_school_tv_name);
        addressTv = itemView.findViewById(R.id.item_high_school_tv_address);
        overviewTv = itemView.findViewById(R.id.item_high_school_tv_overview);
    }

    void bind(@NonNull HighSchoolUiEntity uiEntity) {

        nameTv.setText(uiEntity.getName());
        addressTv.setText(uiEntity.getAddress());
        overviewTv.setText(uiEntity.getOverview());

        rootCl.setOnClickListener(v -> {
            if (clickListener != null){
                clickListener.onHighSchoolClicked(uiEntity);
            }
        });
    }

}
