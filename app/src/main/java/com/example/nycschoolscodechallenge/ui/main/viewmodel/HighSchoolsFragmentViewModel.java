package com.example.nycschoolscodechallenge.ui.main.viewmodel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.PagedList;

import com.example.nycschoolscodechallenge.data.HighSchoolsRepository;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.ui.main.entity.HighSchoolUiEntity;
import com.example.nycschoolscodechallenge.viewmodel.SingleLiveEvent;

import javax.inject.Inject;

import timber.log.Timber;

public class HighSchoolsFragmentViewModel extends ViewModel {

    private static final  String MESSAGE_USER_ERROR = "Error loading data\nPlease check your " +
            "internet connection\nand try again";
    private static final String MESSAGE_INTERNAL_ERROR = "Error getting data for high schools fragment";

    private final LiveData<PagedList<HighSchoolEntity>> highSchoolsPagedLiveData;
    private final MutableLiveData<String> userErrorMessageLiveData;
    private final SingleLiveEvent<HighSchoolUiEntity> onHighSchoolClickedLiveEvent;

    private final HighSchoolsRepository repository;

    private MediatorLiveData<STATE> stateLiveData;

    @Inject
    HighSchoolsFragmentViewModel(@NonNull HighSchoolsRepository repository){

        this.repository = repository;
        this.highSchoolsPagedLiveData = repository.getHighSchoolsPagedLivedData();
        this.userErrorMessageLiveData = new MutableLiveData<>();
        this.stateLiveData = new MediatorLiveData<>();
        this.onHighSchoolClickedLiveEvent = new SingleLiveEvent<>();
    }

    public void start() {

        stateLiveData.setValue(STATE.LOADING);

        userErrorMessageLiveData.setValue(MESSAGE_USER_ERROR);

        stateLiveData.removeSource(repository.getDataSourceStateLiveData());
        stateLiveData.addSource(repository.getDataSourceStateLiveData(), dataSourceState -> {

            switch (dataSourceState){

                case LOADED:
                    handleDataSourceStateLoaded();
                    break;

                case ERROR:
                    handleDataSourceStateError();
                    break;

                case LOADING:
                    default:
                    break;
            }
        });
    }

    public void onHighSchoolClicked(@NonNull HighSchoolUiEntity highSchoolUiEntity) {

        onHighSchoolClickedLiveEvent.setValue(highSchoolUiEntity);
    }

    public void onTryAgainBtnClicked() {

        stateLiveData.setValue(STATE.LOADING);
        repository.refreshDataSource();
    }

    public LiveData<PagedList<HighSchoolEntity>> getHighSchoolsPagedLiveData() {

        return highSchoolsPagedLiveData;
    }

    public MediatorLiveData<STATE> getStateLiveData() {

        return stateLiveData;
    }

    public MutableLiveData<String> getUserErrorMessageLiveData() {

        return userErrorMessageLiveData;
    }

    public SingleLiveEvent<HighSchoolUiEntity> getOnHighSchoolClickedLiveEvent() {

        return onHighSchoolClickedLiveEvent;
    }

    private void handleDataSourceStateLoaded() {

        stateLiveData.setValue(STATE.LOADED);
    }

    private void handleDataSourceStateError() {

        Timber.e(MESSAGE_INTERNAL_ERROR);
        stateLiveData.setValue(STATE.ERROR);
    }

    public enum STATE {

        LOADING, LOADED, ERROR
    }

}
