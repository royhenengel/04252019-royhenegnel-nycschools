package com.example.nycschoolscodechallenge.data.source.remote;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.nycschoolscodechallenge.BuildConfig;
import com.example.nycschoolscodechallenge.api.HighSchoolsService;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.data.source.remote.mapper.HighSchoolResponseItemToEntityMapper;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class HighSchoolsRemoteDataSourceImpl extends HighSchoolsRemoteDataSource {

    private Disposable disposableDataInitial;
    private Disposable disposableDataBefore;
    private Disposable disposableDataAfter;

    HighSchoolsRemoteDataSourceImpl(@NonNull HighSchoolsService service,
                                    @NonNull HighSchoolResponseItemToEntityMapper highSchoolResponseItemToEntityMapper) {
        super(service,
              highSchoolResponseItemToEntityMapper);
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params,
                            @NonNull LoadInitialCallback<Integer, HighSchoolEntity> callback) {

        stateLiveData.postValue(STATE.LOADING);
        disposableDataInitial = remoteService.getHighSchoolsSingle(BuildConfig.API_KEY, RESULTS_LIMIT, 0)
                             .map(this::mapHighSchoolResponseItemsToEntities)
                             .subscribe(highSchoolEntities -> handleLoadInitialData(highSchoolEntities, callback),
                                        this::handleError);

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params,
                           @NonNull LoadCallback<Integer, HighSchoolEntity> callback) {

        stateLiveData.postValue(STATE.LOADING);
        disposableDataBefore = remoteService.getHighSchoolsSingle(BuildConfig.API_KEY, RESULTS_LIMIT, params.key)
                                            .map(this::mapHighSchoolResponseItemsToEntities)
                                            .subscribe(highSchoolResponseItems ->
                                                               handleLoadDataBefore(highSchoolResponseItems, params, callback),
                                                       this::handleError);
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params,
                          @NonNull LoadCallback<Integer, HighSchoolEntity> callback) {

        stateLiveData.postValue(STATE.LOADING);
        disposableDataAfter = remoteService.getHighSchoolsSingle(BuildConfig.API_KEY, RESULTS_LIMIT, params.key)
                                           .map(this::mapHighSchoolResponseItemsToEntities)
                                           .subscribe(highSchoolResponseItems ->
                                                              handleLoadDataAfter(highSchoolResponseItems, params, callback),
                                                      this::handleError);
    }

    @Override
    public void invalidate() {

        if (disposableDataInitial != null) {
            disposableDataInitial.dispose();
        }

        if (disposableDataBefore != null) {
            disposableDataBefore.dispose();
        }

        if (disposableDataAfter != null) {
            disposableDataAfter.dispose();
        }

        super.invalidate();
    }

    private void handleLoadInitialData(@Nullable List<HighSchoolEntity> highSchoolResponseItems,
                                       @NotNull LoadInitialCallback<Integer, HighSchoolEntity> callback) {

        if (highSchoolResponseItems != null) {
            callback.onResult(highSchoolResponseItems,
                              null,
                              RESULTS_LIMIT);
            stateLiveData.postValue(STATE.LOADED);
        }
        else {
            handleError(new NullPointerException(ERROR_ITEMS_CANNOT_BE_NULL));
        }
    }

    private void handleLoadDataBefore(@Nullable List<HighSchoolEntity> highSchoolResponseItems,
                                      @NonNull LoadParams<Integer> params,
                                      @NotNull LoadCallback<Integer, HighSchoolEntity> callback) {

        if (highSchoolResponseItems != null) {
            callback.onResult(highSchoolResponseItems,
                              keyBefore(params));
            stateLiveData.postValue(STATE.LOADED);
        }
        else {
            handleError(new NullPointerException(ERROR_ITEMS_CANNOT_BE_NULL));
        }
    }

    private void handleLoadDataAfter(@Nullable List<HighSchoolEntity> highSchoolResponseItems,
                                     @NonNull LoadParams<Integer> params,
                                     @NonNull LoadCallback<Integer, HighSchoolEntity> callback) {

        if (highSchoolResponseItems != null) {
            callback.onResult(highSchoolResponseItems,
                              keyAfter(params));
            stateLiveData.postValue(STATE.LOADED);
        }
        else {
            handleError(new NullPointerException(ERROR_ITEMS_CANNOT_BE_NULL));
        }
    }

    private void handleError(Throwable throwable) {

        Timber.e(throwable);
        stateLiveData.postValue(STATE.ERROR);
    }

}
