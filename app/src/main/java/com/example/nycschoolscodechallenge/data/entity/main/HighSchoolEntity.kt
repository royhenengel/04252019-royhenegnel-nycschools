package com.example.nycschoolscodechallenge.data.entity.main

data class HighSchoolEntity(

        val id : String,

        val name: String,

        val street: String,

        val city: String,

        val stateCode: String,

        val zipCode: String,

        val website: String,

        val overview: String

)