package com.example.nycschoolscodechallenge.data.source.remote;

import androidx.annotation.NonNull;

import com.example.nycschoolscodechallenge.BuildConfig;
import com.example.nycschoolscodechallenge.api.HighSchoolsService;
import com.example.nycschoolscodechallenge.api.details.entity.HighSchoolSatScoresResponseItem;
import com.example.nycschoolscodechallenge.data.entity.details.HighSchoolSatScoresEntity;
import com.example.nycschoolscodechallenge.data.source.remote.mapper.HighSchoolSatScoreResponseItemToEntityMapper;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class HighSchoolSatScoresRemoteDataSource {

    private final HighSchoolsService service;
    private final HighSchoolSatScoreResponseItemToEntityMapper highSchoolSatScoreResponseItemToEntityMapper;

    @Inject
    public HighSchoolSatScoresRemoteDataSource(@NonNull HighSchoolsService schoolsService,
                                               @NonNull HighSchoolSatScoreResponseItemToEntityMapper highSchoolSatScoreResponseItemToEntityMapper) {

        this.service = schoolsService;
        this.highSchoolSatScoreResponseItemToEntityMapper = highSchoolSatScoreResponseItemToEntityMapper;
    }

    public Single<HighSchoolSatScoresEntity> getHighSchoolScores(@NonNull String schoolId) {

        return service.getHighSchoolSatScores(BuildConfig.API_KEY, schoolId)
                .map(highSchoolSatScoresResponseItems -> {
                    // List should always return a list with a single item.
                    // If the returned list is empty, the school wasn't found in the API.
                    if (highSchoolSatScoresResponseItems.size() == 1){
                        return highSchoolSatScoresResponseItems.get(0);
                    }
                    else {
                        throw new IndexOutOfBoundsException();
                    }
                })
                .map(highSchoolSatScoreResponseItemToEntityMapper);
    }

}
