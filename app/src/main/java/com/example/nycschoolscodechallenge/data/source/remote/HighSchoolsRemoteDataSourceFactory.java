package com.example.nycschoolscodechallenge.data.source.remote;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.example.nycschoolscodechallenge.api.HighSchoolsService;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.data.source.remote.mapper.HighSchoolResponseItemToEntityMapper;

import javax.inject.Inject;

public class HighSchoolsRemoteDataSourceFactory extends DataSource.Factory<Integer, HighSchoolEntity> {

    private final HighSchoolsService service;
    private final HighSchoolResponseItemToEntityMapper highSchoolResponseItemToEntityMapper;

    private MutableLiveData<HighSchoolsRemoteDataSource> dataSourceLiveData;

    @Inject
    public HighSchoolsRemoteDataSourceFactory(@NonNull HighSchoolsService service,
                                              @NonNull HighSchoolResponseItemToEntityMapper highSchoolResponseItemToEntityMapper) {

        this.service = service;
        this.highSchoolResponseItemToEntityMapper = highSchoolResponseItemToEntityMapper;
        this.dataSourceLiveData = new MutableLiveData<>();
    }

    @Override
    public DataSource<Integer, HighSchoolEntity> create() {

        final HighSchoolsRemoteDataSource dataSource =
                new HighSchoolsRemoteDataSourceImpl(service, highSchoolResponseItemToEntityMapper);

        dataSourceLiveData.postValue(dataSource);

        return dataSource;
    }

    public MutableLiveData<HighSchoolsRemoteDataSource> getDataSourceLiveData(){

        return dataSourceLiveData;
    }

}
