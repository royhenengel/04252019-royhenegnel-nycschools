package com.example.nycschoolscodechallenge.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.nycschoolscodechallenge.api.details.entity.HighSchoolSatScoresResponseItem;
import com.example.nycschoolscodechallenge.data.entity.details.HighSchoolSatScoresEntity;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.data.source.remote.HighSchoolSatScoresRemoteDataSource;
import com.example.nycschoolscodechallenge.data.source.remote.HighSchoolsRemoteDataSource;
import com.example.nycschoolscodechallenge.data.source.remote.HighSchoolsRemoteDataSourceFactory;
import com.example.nycschoolscodechallenge.data.source.remote.HighSchoolsRemoteDataSourceImpl;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

import static com.example.nycschoolscodechallenge.data.source.remote.HighSchoolsRemoteDataSourceImpl.RESULTS_LIMIT;

public class HighSchoolsRepository {

    private final LiveData<PagedList<HighSchoolEntity>> highSchoolsPagedLivedData;
    private final LiveData<HighSchoolsRemoteDataSourceImpl.STATE> dataSourceStateLiveData;

    private final HighSchoolSatScoresRemoteDataSource satScoresRemoteDataSource;

    @Inject
    HighSchoolsRepository(@NonNull HighSchoolsRemoteDataSourceFactory remoteDataSourceFactory,
                          @NonNull HighSchoolSatScoresRemoteDataSource satScoresRemoteDataSource) {

        this.highSchoolsPagedLivedData = initHighSchoolsPagedLivedData(remoteDataSourceFactory);
        this.dataSourceStateLiveData =
                Transformations.switchMap(remoteDataSourceFactory.getDataSourceLiveData(),
                                                                 HighSchoolsRemoteDataSource::getStateLiveData);

        this.satScoresRemoteDataSource = satScoresRemoteDataSource;
    }

    public LiveData<PagedList<HighSchoolEntity>> getHighSchoolsPagedLivedData() {

        return highSchoolsPagedLivedData;
    }

    public LiveData<HighSchoolsRemoteDataSourceImpl.STATE> getDataSourceStateLiveData() {

        return dataSourceStateLiveData;
    }

    @NotNull
    private LiveData<PagedList<HighSchoolEntity>> initHighSchoolsPagedLivedData(
            @NonNull HighSchoolsRemoteDataSourceFactory remoteDataSourceFactory) {

        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(RESULTS_LIMIT)
                .build();

        return new LivePagedListBuilder<>(remoteDataSourceFactory,
                                          config).build();
    }

    public Single<HighSchoolSatScoresEntity> getHighSchoolScores(String schoolId) {

        return satScoresRemoteDataSource.getHighSchoolScores(schoolId);
    }

    public void refreshDataSource() {

        highSchoolsPagedLivedData.getValue().getDataSource().invalidate();
    }
}
