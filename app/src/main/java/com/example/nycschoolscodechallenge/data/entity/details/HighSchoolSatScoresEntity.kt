package com.example.nycschoolscodechallenge.data.entity.details

data class HighSchoolSatScoresEntity (

        val id: String,

        val schoolName: String,

        val writingAvgScore: String,

        val readingAvgScore: String,

        val mathAvgScore: String

)