package com.example.nycschoolscodechallenge.data.source.remote.mapper;

import com.example.nycschoolscodechallenge.api.details.entity.HighSchoolSatScoresResponseItem;
import com.example.nycschoolscodechallenge.api.main.entity.HighSchoolResponseItem;
import com.example.nycschoolscodechallenge.data.entity.details.HighSchoolSatScoresEntity;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;

import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.functions.Function;

public class HighSchoolSatScoreResponseItemToEntityMapper implements Function<HighSchoolSatScoresResponseItem, HighSchoolSatScoresEntity> {

    @Inject
    public HighSchoolSatScoreResponseItemToEntityMapper() { }

    @Override
    public HighSchoolSatScoresEntity apply(HighSchoolSatScoresResponseItem highSchoolResponseItem) {

        return new HighSchoolSatScoresEntity(
                Objects.requireNonNull(highSchoolResponseItem.getDbn()),
                Objects.requireNonNull(highSchoolResponseItem.getSchoolName()),
                Objects.requireNonNull(highSchoolResponseItem.getSatWritingAvgScore()),
                Objects.requireNonNull(highSchoolResponseItem.getSatCriticalReadingAvgScore()),
                Objects.requireNonNull(highSchoolResponseItem.getSatMathAvgScore()));
    }

}
