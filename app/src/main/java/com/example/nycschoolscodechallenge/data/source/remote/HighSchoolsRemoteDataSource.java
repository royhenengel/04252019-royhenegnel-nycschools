package com.example.nycschoolscodechallenge.data.source.remote;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import com.example.nycschoolscodechallenge.api.HighSchoolsService;
import com.example.nycschoolscodechallenge.api.main.entity.HighSchoolResponseItem;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;
import com.example.nycschoolscodechallenge.data.source.remote.mapper.HighSchoolResponseItemToEntityMapper;

import java.util.ArrayList;
import java.util.List;

public abstract class HighSchoolsRemoteDataSource extends PageKeyedDataSource<Integer, HighSchoolEntity> {

    public static final int RESULTS_LIMIT = 20;

    static final String ERROR_ITEMS_CANNOT_BE_NULL = "List cannot be null";

    MutableLiveData<STATE> stateLiveData;

    final HighSchoolsService remoteService;
    private final HighSchoolResponseItemToEntityMapper highSchoolResponseItemToEntityMapper;

    HighSchoolsRemoteDataSource(@NonNull HighSchoolsService remoteService,
                                       @NonNull HighSchoolResponseItemToEntityMapper highSchoolResponseItemToEntityMapper) {

        this.remoteService = remoteService;
        this.highSchoolResponseItemToEntityMapper = highSchoolResponseItemToEntityMapper;
        this.stateLiveData = new MutableLiveData<>();
    }

    public abstract void loadInitial(@NonNull LoadInitialParams<Integer> params,
                                     @NonNull LoadInitialCallback<Integer, HighSchoolEntity> callback);

    public abstract void loadBefore(@NonNull LoadParams<Integer> params,
                                    @NonNull LoadCallback<Integer, HighSchoolEntity> callback);

    public abstract void loadAfter(@NonNull LoadParams<Integer> params,
                                   @NonNull LoadCallback<Integer, HighSchoolEntity> callback);

    List<HighSchoolEntity> mapHighSchoolResponseItemsToEntities(@Nullable List<HighSchoolResponseItem> responseItems) {

        ArrayList<HighSchoolEntity> entities = new ArrayList<>();
        if (responseItems != null){
            for (HighSchoolResponseItem highSchoolResponseItem : responseItems){
                entities.add(highSchoolResponseItemToEntityMapper.apply(highSchoolResponseItem));
            }
        }
        return entities;
    }

    @Nullable
    Integer keyBefore(@NonNull LoadParams<Integer> params) {

        if (params.key > 1) {
            return params.key - RESULTS_LIMIT;
        }
        else {
            return null;
        }
    }

    @Nullable
    Integer keyAfter(@NonNull LoadParams<Integer> params) {

        if (params.key > 1) {
            return params.key + RESULTS_LIMIT;
        }
        else {
            return null;
        }
    }

    public MutableLiveData<STATE> getStateLiveData() {

        return stateLiveData;
    }

    public enum STATE {

        LOADING, LOADED, ERROR
    }
}
