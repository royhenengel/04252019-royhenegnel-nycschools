package com.example.nycschoolscodechallenge.data.source.remote.mapper;

import com.example.nycschoolscodechallenge.api.main.entity.HighSchoolResponseItem;
import com.example.nycschoolscodechallenge.data.entity.main.HighSchoolEntity;

import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.functions.Function;

public class HighSchoolResponseItemToEntityMapper implements Function<HighSchoolResponseItem, HighSchoolEntity> {

    @Inject
    public HighSchoolResponseItemToEntityMapper() { }

    @Override
    public HighSchoolEntity apply(HighSchoolResponseItem highSchoolResponseItem) {

        return new HighSchoolEntity(
                Objects.requireNonNull(highSchoolResponseItem.getDbn()),
                Objects.requireNonNull(highSchoolResponseItem.getSchoolName()),
                Objects.requireNonNull(highSchoolResponseItem.getPrimaryAddressLine1()),
                Objects.requireNonNull(highSchoolResponseItem.getCity()),
                Objects.requireNonNull(highSchoolResponseItem.getStateCode()),
                Objects.requireNonNull(highSchoolResponseItem.getZip()),
                Objects.requireNonNull(highSchoolResponseItem.getWebsite()),
                Objects.requireNonNull(highSchoolResponseItem.getOverviewParagraph())
        );
    }

}
